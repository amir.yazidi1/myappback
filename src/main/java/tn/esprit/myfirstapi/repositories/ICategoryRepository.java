package tn.esprit.myfirstapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.myfirstapi.models.Category;

@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long> {
}
