package tn.esprit.myfirstapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.myfirstapi.models.Order;

@Repository
public interface IOrderRepository extends JpaRepository<Order, Long> {
}
