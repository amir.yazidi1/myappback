package tn.esprit.myfirstapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.myfirstapi.models.Provider;

@Repository
public interface IProviderRepository extends JpaRepository<Provider, Long> {
}
