package tn.esprit.myfirstapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.myfirstapi.models.Product;

@Repository
public interface IProductRepository extends JpaRepository<Product, Long> {
}
