package tn.esprit.myfirstapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.esprit.myfirstapi.models.SubCategory;

@Repository
public interface ISubCategoryRepository extends JpaRepository<SubCategory, Long> {
}
