package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.Category;

import java.util.List;

public interface ICategoryService {

    Category addCategory(Category c);

    Category updateCategoryById(Category c);

    List<Category> getAllCategory();

    Category retrieveCategory(Long id);

    Boolean deleteCategoryById(Long id);
}
