package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.Product;

import java.util.List;

public interface IProductService {

    Product addProduct(Product p);

    Product updateProductById(Product p);

    List<Product> getAllProducts();

    Product retrieveProduct(Long id);

    Boolean deleteProductById(Long id);
}
