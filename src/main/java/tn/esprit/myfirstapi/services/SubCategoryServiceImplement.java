package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Provider;
import tn.esprit.myfirstapi.models.SubCategory;
import tn.esprit.myfirstapi.repositories.ISubCategoryRepository;

import java.util.List;

@Service
public class SubCategoryServiceImplement implements ISubCategoryService{

    @Autowired
    private ISubCategoryRepository iSubCategoryRepository;
    @Override
    public SubCategory addSubCategory(SubCategory sc) {
        return iSubCategoryRepository.save(sc);
    }

    @Override
    public SubCategory updateSubCategoryById(SubCategory sc) {
        return iSubCategoryRepository.saveAndFlush(sc);
    }

    @Override
    public List<SubCategory> getAllSubCategorys() {
        return iSubCategoryRepository.findAll();
    }

    @Override
    public SubCategory retrieveSubCategory(Long id) {
        SubCategory sc = iSubCategoryRepository.findById((id)).get();
        return sc;
    }

    @Override
    public Boolean deleteSubCategoryById(Long id) {
        try {
            iSubCategoryRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
