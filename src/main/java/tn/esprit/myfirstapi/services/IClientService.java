package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.Client;

import java.util.List;

public interface IClientService {

    Client addClient(Client cl);

    Client updateClientById(Client cl);

    List<Client> getAllClients();

    Client retrieveClient(Long id);

    Boolean deleteClientById(Long id);
}
