package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.util.Password;
import tn.esprit.myfirstapi.models.SubCategory;
import tn.esprit.myfirstapi.models.User;
import tn.esprit.myfirstapi.repositories.IUserRepository;

import javax.validation.constraints.Email;
import java.util.List;

@Service
public class UserServiceImplement implements IUserService {
    @Autowired  //injection
    private IUserRepository iUserRepository;

    @Override
    public User addUser(User u) {
        return iUserRepository.save(u);
    }

    @Override
    public User updateUserById(User u) {
        return iUserRepository.saveAndFlush(u);
    }

    @Override
    public List<User> getAllUsers() {
        return iUserRepository.findAll();
    }

    @Override
    public User retrieveUser(Long id) {
        User u = iUserRepository.findById((id)).get();
        return u;     }

    @Override
    public Boolean deleteUserById(Long id) {

        try {
            iUserRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public User login(String email,String password) {
        return iUserRepository.login(email, password);
    }
}
