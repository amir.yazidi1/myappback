package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.Order;

import java.util.List;

public interface IOrderService {

    Order addOrder(Order o);

    Order updateOrderById(Order o);

    List<Order> getAllOrders();

    Order retrieveOrder(Long id);

    Boolean deleteOrderById(Long id);
}
