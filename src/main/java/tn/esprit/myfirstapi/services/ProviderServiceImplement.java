package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Product;
import tn.esprit.myfirstapi.models.Provider;
import tn.esprit.myfirstapi.repositories.IProviderRepository;

import java.util.List;

@Service
public class ProviderServiceImplement implements IProviderService {

    @Autowired
    private IProviderRepository iProviderRepository;

    @Override
    public Provider addProvider(Provider prov) {
        return iProviderRepository.save(prov);
    }

    @Override
    public Provider updateProviderById(Provider prov) {
        return iProviderRepository.saveAndFlush(prov);
    }

    @Override
    public List<Provider> getAllProviders() {
        return iProviderRepository.findAll();
    }

    @Override
    public Provider retrieveProvider(Long id) {
        Provider pro = iProviderRepository.findById((id)).get();
        return pro;
    }

    @Override
    public Boolean deleteProviderById(Long id) {
        try {
            iProviderRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
