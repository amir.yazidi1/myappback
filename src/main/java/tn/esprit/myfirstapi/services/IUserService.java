package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.User;

import java.util.List;

public interface IUserService {
    User addUser(User u);

    User updateUserById(User u);

    List<User> getAllUsers();

    User retrieveUser(Long id);

    Boolean deleteUserById(Long id);

    User login(String email,String password);
}
