package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.Provider;

import java.util.List;

public interface IProviderService {

    Provider addProvider(Provider prov);

    Provider updateProviderById(Provider prov);

    List<Provider> getAllProviders();

    Provider retrieveProvider(Long id);

    Boolean deleteProviderById(Long id);
}
