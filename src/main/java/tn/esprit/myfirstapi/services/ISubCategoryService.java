package tn.esprit.myfirstapi.services;

import tn.esprit.myfirstapi.models.SubCategory;

import java.util.List;

public interface ISubCategoryService {

    SubCategory addSubCategory(SubCategory sc);

    SubCategory updateSubCategoryById(SubCategory sc);

    List<SubCategory> getAllSubCategorys();

    SubCategory retrieveSubCategory(Long id);

    Boolean deleteSubCategoryById(Long id);
}
