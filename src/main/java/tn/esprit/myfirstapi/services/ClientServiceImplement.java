package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Client;
import tn.esprit.myfirstapi.repositories.IClientRepository;

import java.util.List;
@Service
public class ClientServiceImplement implements IClientService{
    @Autowired
    private IClientRepository iClientRepository;

    @Override
    public Client addClient(Client cl) {
        return iClientRepository.save(cl);
    }

    @Override
    public Client updateClientById(Client cl) {
        return iClientRepository.saveAndFlush(cl);
    }

    @Override
    public List<Client> getAllClients() {
        return iClientRepository.findAll();
    }

    @Override
    public Client retrieveClient(Long id) {
        Client c = iClientRepository.findById((id)).get();
        return c;
    }

    @Override
    public Boolean deleteClientById(Long id) {

        try {
            iClientRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
