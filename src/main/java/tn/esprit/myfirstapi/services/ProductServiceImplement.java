package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Order;
import tn.esprit.myfirstapi.models.Product;
import tn.esprit.myfirstapi.repositories.IProductRepository;

import java.util.List;
@Service
public class ProductServiceImplement implements IProductService {
    @Autowired
    private IProductRepository iProductRepository;
    @Override
    public Product addProduct(Product p) {
        return iProductRepository.save(p);
    }

    @Override
    public Product updateProductById(Product p) {
        return iProductRepository.saveAndFlush(p);
    }

    @Override
    public List<Product> getAllProducts() {
        return iProductRepository.findAll();
    }

    @Override
    public Product retrieveProduct(Long id) {
        Product p = iProductRepository.findById((id)).get();
        return p;
    }

    @Override
    public Boolean deleteProductById(Long id) {
        try {
            iProductRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
