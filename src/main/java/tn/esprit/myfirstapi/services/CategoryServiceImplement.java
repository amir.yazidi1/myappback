package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Category;
import tn.esprit.myfirstapi.repositories.ICategoryRepository;

import java.util.List;

@Service
public class CategoryServiceImplement implements ICategoryService{
    @Autowired
    private ICategoryRepository iCategoryRepository;
    @Override
    public Category addCategory(Category c) {
        return iCategoryRepository.save(c);
    }

    @Override
    public Category updateCategoryById(Category c) {
        return iCategoryRepository.saveAndFlush(c);
    }

    @Override
    public List<Category> getAllCategory() {
        return iCategoryRepository.findAll();
    }

    @Override
    public Category retrieveCategory(Long id) {
        Category cat = iCategoryRepository.findById((id)).get();
        return cat;
    }

    @Override
    public Boolean deleteCategoryById(Long id) {

        try {
            iCategoryRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
