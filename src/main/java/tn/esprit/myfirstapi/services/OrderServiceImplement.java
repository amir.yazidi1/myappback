package tn.esprit.myfirstapi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.myfirstapi.models.Order;
import tn.esprit.myfirstapi.repositories.IOrderRepository;

import java.util.List;
@Service
public class OrderServiceImplement implements IOrderService{
    @Autowired
    private IOrderRepository iOrderRepository;
    @Override
    public Order addOrder(Order o) {
        return iOrderRepository.save(o);
    }

    @Override
    public Order updateOrderById(Order o) {
        return iOrderRepository.saveAndFlush(o);
    }

    @Override
    public List<Order> getAllOrders() {
        return iOrderRepository.findAll();
    }

    @Override
    public Order retrieveOrder(Long id) {
        Order o = iOrderRepository.findById((id)).get();
        return o;
    }

    @Override
    public Boolean deleteOrderById(Long id) {
        try {
            iOrderRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
