package tn.esprit.myfirstapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tn.esprit.myfirstapi.utils.StorageService;

@SpringBootApplication
public class MyfirstapiApplication implements CommandLineRunner {
    @Autowired
    private StorageService storageService;

    public static void main(String[] args) {
        SpringApplication.run(MyfirstapiApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
       // storageService.deleteAll();
     //   storageService.init();
    }
}
