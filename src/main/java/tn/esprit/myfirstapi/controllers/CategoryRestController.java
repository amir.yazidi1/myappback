package tn.esprit.myfirstapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.myfirstapi.models.Category;
import tn.esprit.myfirstapi.models.Client;
import tn.esprit.myfirstapi.services.ICategoryService;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/category")
public class CategoryRestController {

    @Autowired
    private ICategoryService iCategoryService;

    @GetMapping("/")
    public List<Category>getAll(){
        return iCategoryService.getAllCategory();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Category getCategoryById(@PathVariable("id") Long categoryId) {
        return iCategoryService.retrieveCategory(categoryId);
    }

    @PostMapping("/")
    public Category addCategory(@RequestBody Category c){
        return iCategoryService.addCategory(c);
    }
    @PutMapping("/")
    public Category updateCategory(@RequestBody Category c){
        return iCategoryService.updateCategoryById(c);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteCategory(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iCategoryService.deleteCategoryById(id));
            return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iCategoryService.deleteCategoryById(id));
            return hashMap;

        }

    }

}
