package tn.esprit.myfirstapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tn.esprit.myfirstapi.models.Product;
import tn.esprit.myfirstapi.models.User;
import tn.esprit.myfirstapi.services.IUserService;
import tn.esprit.myfirstapi.utils.StorageService;

import javax.ws.rs.PathParam;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@RestController
@CrossOrigin("*")
@RequestMapping("/users")
public class UserRestController {
    @Autowired
    private IUserService iUserService;
    @Autowired
    private StorageService storageService;

    @GetMapping("/")
    public List<User>getAll(){
        return iUserService.getAllUsers();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public User getUserById(@PathVariable("id") Long userId) {
        return iUserService.retrieveUser(userId);
    }
    @PostMapping("/")
    public User addUser(User u,@RequestParam("img") MultipartFile img){

        Random rand = new Random();

        // Generate random integers in range 0 to 999
        int rand_int1 = rand.nextInt(1000);

        storageService.store(img, rand_int1+img.getOriginalFilename());
        u.setPhoto(rand_int1+img.getOriginalFilename());
        return iUserService.addUser(u);
    }
    @PutMapping("/")
    public User updateUser(@RequestBody User u){
        return iUserService.updateUserById(u);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteUser(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
             hashMap.put("state", iUserService.deleteUserById(id));
             return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iUserService.deleteUserById(id));
            return hashMap;

        }

    }
    @PostMapping("/login")
    public User login(@RequestBody User u){

        System.out.println(u.getEmail());
        System.out.println(u.getPassword());
        return iUserService.login(u.getEmail(),u.getPassword());
    }

}
