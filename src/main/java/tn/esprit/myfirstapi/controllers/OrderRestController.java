package tn.esprit.myfirstapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.myfirstapi.models.Order;
import tn.esprit.myfirstapi.services.IOrderService;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/orders")
public class OrderRestController {
    @Autowired
    private IOrderService iOrderService;

    @GetMapping("/")
    public List<Order>getAll(){
        return iOrderService.getAllOrders();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Order getOrderById(@PathVariable("id") Long orderId) {
        return iOrderService.retrieveOrder(orderId);
    }

    @PostMapping("/")
    public Order addOrder(@RequestBody Order o){
        return iOrderService.updateOrderById(o);
    }

    @PutMapping("/")
    public Order updateOrder(@RequestBody Order o){
        return iOrderService.updateOrderById(o);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteOrder(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iOrderService.deleteOrderById(id));
            return hashMap;
        }catch(Exception e){
            hashMap.put("state", iOrderService.deleteOrderById(id));
            return hashMap;
        }
    }
}
