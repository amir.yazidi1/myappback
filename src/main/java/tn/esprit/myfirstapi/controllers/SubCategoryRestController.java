package tn.esprit.myfirstapi.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.myfirstapi.models.Provider;
import tn.esprit.myfirstapi.models.SubCategory;
import tn.esprit.myfirstapi.services.ISubCategoryService;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/subCategory")
public class SubCategoryRestController {

    @Autowired
    private ISubCategoryService iSubCategoryService;
    @GetMapping("/")
    public List<SubCategory>getAll(){
        return iSubCategoryService.getAllSubCategorys();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public SubCategory getSubCategoryById(@PathVariable("id") Long subCategoryId) {
        return iSubCategoryService.retrieveSubCategory(subCategoryId);
    }
    @PostMapping("/")
    public SubCategory addSubCategory(@RequestBody SubCategory cl){
        return iSubCategoryService.addSubCategory(cl);
    }
    @PutMapping("/")
    public SubCategory updateSubCategory(@RequestBody SubCategory cl){
        return iSubCategoryService.updateSubCategoryById(cl);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteSubCategory(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iSubCategoryService.deleteSubCategoryById(id));
            return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iSubCategoryService.deleteSubCategoryById(id));
            return hashMap;

        }

    }
}
