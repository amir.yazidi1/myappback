package tn.esprit.myfirstapi.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.myfirstapi.models.Provider;
import tn.esprit.myfirstapi.services.IProviderService;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/Providers")
public class ProviderRestController {

    @Autowired
    private IProviderService iProviderService;
    @GetMapping("/")
    public List<Provider>getAll(){
        return iProviderService.getAllProviders();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Provider getProviderById(@PathVariable("id") Long providerId) {
        return iProviderService.retrieveProvider(providerId);
    }
    @PostMapping("/")
    public Provider addProvider(@RequestBody Provider prov){
        return iProviderService.addProvider(prov);
    }
    @PutMapping("/")
    public Provider updateProvider(@RequestBody Provider prov){
        return iProviderService.updateProviderById(prov);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteProvider(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iProviderService.deleteProviderById(id));
            return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iProviderService.deleteProviderById(id));
            return hashMap;

        }

    }

}
