package tn.esprit.myfirstapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.myfirstapi.models.Client;
import tn.esprit.myfirstapi.models.Product;
import tn.esprit.myfirstapi.services.IProductService;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/Products")
public class ProductRestController {

    @Autowired
    private IProductService iProductService;
    @GetMapping("/")
    public List<Product>getAll(){
        return iProductService.getAllProducts();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Product getProductById(@PathVariable("id") Long productId) {
        return iProductService.retrieveProduct(productId);
    }

    @PostMapping("/")
    public Product addProduct(@RequestBody Product p){
        return iProductService.addProduct(p);
    }
    @PutMapping("/")
    public Product updateProduct(@RequestBody Product p){
        return iProductService.updateProductById(p);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteProduct(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iProductService.deleteProductById(id));
            return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iProductService.deleteProductById(id));
            return hashMap;

        }

    }

}
