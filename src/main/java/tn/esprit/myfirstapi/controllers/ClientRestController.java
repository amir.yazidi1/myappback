package tn.esprit.myfirstapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tn.esprit.myfirstapi.models.Client;
import tn.esprit.myfirstapi.services.IClientService;
import tn.esprit.myfirstapi.utils.StorageService;
import org.springframework.core.io.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

@RestController
@CrossOrigin("*")
@RequestMapping("/clients")
public class ClientRestController {

    @Autowired
    private IClientService iClientService;
    @Autowired
    private StorageService storageService;

    @GetMapping("/")
    public List<Client>getAll(){
        return iClientService.getAllClients();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Client getClientById(@PathVariable("id") Long clientId) {
        return iClientService.retrieveClient(clientId);
    }

    @PostMapping("/")
    public Client addClient(Client cl,@RequestParam("img") MultipartFile img ){

        Random rand = new Random();

        // Generate random integers in range 0 to 999
        int rand_int1 = rand.nextInt(1000);

        storageService.store(img, rand_int1+img.getOriginalFilename());
        cl.setPhoto(rand_int1+img.getOriginalFilename());
        return iClientService.addClient(cl);
    }
    @PutMapping("/")
    public Client updateClient(@RequestBody Client cl){
        return iClientService.updateClientById(cl);
    }
    @DeleteMapping("/")
    public HashMap<String,Boolean> deleteClient(Long id){
        HashMap<String,Boolean> hashMap=new HashMap<>();
        try{
            hashMap.put("state", iClientService.deleteClientById(id));
            return hashMap;

        }catch (Exception e){
            hashMap.put("state",  iClientService.deleteClientById(id));
            return hashMap;

        }

    }
    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.loadFile(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
